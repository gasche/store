.PHONY: all random fuzz

all:
	dune build

random:
	dune build @fuzzing/random --no-buffer

fuzz:
	dune build @fuzzing/fuzz --no-buffer

run-bench:
	dune build --profile=release bench/bench.exe
	dune exec --profile=release bench/bench.exe
