(* a Store interface defined on top of fcl_stak.ml *)

module Fcl = Facile.Fcl_stak

(* We are cheating here as [Fcl_stak] only supports one global store.
   This is incorrect but good enough to do benchmarks. *)
type 'a store = unit
type 'a t = 'a store

let create () =
  Fcl.reset ()

module Ref = struct
  type 'a t = 'a Fcl.ref

  let[@inline always] make () v = Fcl.ref v
  let[@inline always] get () r = Fcl.get r 
  let[@inline always] set () r v = Fcl.set r v
end

let tentatively () f =
  let saved = Fcl.level () in
  let _ = Fcl.save [] in
  match f () with
  | v ->
     Fcl.cut saved;
     v
  | exception e ->
     let b = Printexc.get_raw_backtrace() in
     ignore (Fcl.backtrack ());
     Printexc.raise_with_backtrace e b

