let () =
  let n =
    try int_of_string @@ Sys.getenv "ALIGNMENT_FUZZ" with Not_found -> 1
  in
  let f = open_in Sys.argv.(1) in
  try
    while true do
      print_endline @@ input_line f
    done
  with End_of_file ->
    print_endline "let () =";
    print_string "  ignore (Sys.opaque_identity List.nth (Sys.opaque_identity [0";
    for i = 1 to n do
      print_string "; ";
      print_int i
    done;
    print_endline "]) 0)"
