(** A benchmark for the various implementations of reference stores
    (mutable, copyable, persistent etc.).

    Documentation comments throughout the code explain the user-facing interface. *)

(** {3 Common benchmark parameters.} *)

type log_count = Log of count [@@unboxed]
and count = int

(** Most benchmark parameters are given in log-value:
    NREADS=10 means 2^10 reference reads. *)
let from_log (Log n) =
  let res = 1 lsl n in
  let nmax =
    (* 1 lsl int_size is 0,
       1 lsl (int_size - 1) is negative. *)
    Sys.int_size - 2 in
  if n >= nmax then
    Printf.ksprintf failwith
      "from_log: unsupported log-value %d (maximum: %d)"
      n nmax;
  assert (0 < res && res <= max_int);
  res


type config = {
  ncreate : log_count;
  (** [ncreate] is the log-count of reference creations to perform. *)

  nread : log_count;
  (** [nread] is the log-count of reference reads to perform. *)

  nwrite : log_count;
  (** [nwrite] is the log-count of reference writes to perform. *)

  rounds : count;
  (** [rounds] is a multiplicative factor, the number of repetitions of
     the benchmark. It is a count, not a log-count. This gives more
     flexibility to compare implementations with different runtimes
     without blowing up the benchmark time: if an implementation is 10x
     slower, you can easily run it with 10x less rounds. *)
}

(** {3 Benchmarks and implementations.} *)

(* Supported benchmarks *)
type bench =
  | Raw
    (* Just get and set. *)

  | Transaction of [ `Raw | `Commit | `Abort ]
    (* Benchmark under transactions. The `Raw variant only runs the
       Raw benchmark under a transaction, the `Commit and `Abort variant
       performs O(rounds) transactions. *)

  | Backtracking of [ `Abort | `Commit ] * [ `Semi_persistent | `Persistent]
    (* - The `Abort benchmark exercises [abort].

       - The `Commit benchmark exercises [commit].

       - The `Persistent variant runs a Backtracking workflow using
         the persistent operations of the implementation. This is useful
         to compare the relative cost of the semi-persistent and the
         persistent interfaces for implementations that provide both. *)

  (* Note: We don't currently have "persistent benchmarks" designed to
     exercise deeply the persistent implementations. Indeed, we have
     only one reasonable implementation that supports this
     interface. Comparing it against itself is not so useful. *)

type raw_impl =
  | Ref
  | TransactionalRef
  | BacktrackingRef
  | Facile
  | Colibri2
  | Store
  | Vector
  | Map

(** The "Raw" benchmark simply checks basic store operations. *)
module[@inline always] BenchStore (Store : Sig.Store) = struct
  let run (store : int Store.t) conf =
    let create_count = from_log conf.ncreate in
    let create_mask = create_count - 1 in
    let read_count = from_log conf.nread in
    let write_count = from_log conf.nwrite in
    let refs = Array.init create_count (fun i -> Store.Ref.make store i) in
    let do_reads refs _round =
      for i = 1 to read_count do
        (* [i land (ncreate - 1)]
           is equivalent to
           [i mod create_count]
         -- the modulo operation would previously dominate the
         benchmark running time... *)
        let r = refs.(i land create_mask) in
        ignore (Sys.opaque_identity (Store.Ref.get (Sys.opaque_identity store) r));
      done
    in
    let do_writes refs round =
      for i = 1 to write_count do
        let r = refs.(i land create_mask) in
        Store.Ref.set store r (round + i);
      done
    in
    let rec loop round rounds_count =
      do_reads refs round;
      do_writes refs round;
      if round = rounds_count then ()
      else loop (round + 1) rounds_count
    in loop 0 conf.rounds

  let main conf =
    run (Sys.opaque_identity (Store.create ())) conf
end

(** Benchmarks for stores supporting the transaction interface *)
module[@inline always] BenchStoreTransactional (Store : Sig.Store_transactional) = struct
  (** The "Transactional-raw" benchmark runs the basic benchmark
      under the scope of a transaction. This lets us measure
      the effectiveness of fast paths when no transactions are
      active, by comparing Raw and Transactional-raw. *)
  let run_raw (store : int Store.t) conf =
    let module B = BenchStore(Store) in
    Store.tentatively store (fun () ->
      B.run store conf
    )

  (** The "Transactional-full" benchmarks runs [O(rounds)] transactions,
      and exercises both succesful and failed transactions. *)
  let run_abort (store : int Store.t) conf =
    let create_count = from_log conf.ncreate in
    let create_mask = create_count - 1 in
    let read_count = from_log conf.nread in
    let write_count = from_log conf.nwrite in
    let refs = Array.init create_count (fun i -> Store.Ref.make store i) in
    let do_reads refs _round =
      for i = 1 to read_count do
        let r = refs.(i land create_mask) in
        ignore (Sys.opaque_identity (Store.Ref.get (Sys.opaque_identity store) r));
      done
    in
    let do_writes refs round =
      for i = 1 to write_count do
        let r = refs.(i land create_mask) in
        Store.Ref.set store r (round + i);
      done
    in
    let rec loop round =
      begin match
          Store.tentatively store (fun () ->
            do_reads refs round;
            do_writes refs round;
            raise Exit
          )
        with
        | () -> assert false
        | exception Exit -> ()
      end;
      if round = conf.rounds then ()
      else loop (round + 1)
    in loop 0

  let run_commit (store : int Store.t) conf =
    let create_count = from_log conf.ncreate in
    let create_mask = create_count - 1 in
    let read_count = from_log conf.nread in
    let write_count = from_log conf.nwrite in
    let refs = Array.init create_count (fun i -> Store.Ref.make store i) in
    let do_reads refs _round =
      for i = 1 to read_count do
        let r = refs.(i land create_mask) in
        ignore (Sys.opaque_identity (Store.Ref.get (Sys.opaque_identity store) r));
      done
    in
    let do_writes refs round =
      for i = 1 to write_count do
        let r = refs.(i land create_mask) in
        Store.Ref.set store r (round + i);
      done
    in
    let rec loop round =
      (* succesful transaction *)
      Store.tentatively store (fun () ->
        do_reads refs round;
        do_writes refs round
      );
      if round = conf.rounds then ()
      else loop (round + 1)
    in loop 0

  let main (mode : [ `Raw | `Commit | `Abort]) conf =
    let run =
      match mode with
      | `Raw -> run_raw
      | `Commit -> run_commit
      | `Abort -> run_abort
    in run (Sys.opaque_identity (Store.create ())) conf
end


module[@inline always] BenchStoreBacktracking (Store : Sig.Store_backtracking) = struct
  (** The "Backtracking" benchmark runs [O(rounds)] backtrack points,
      and exercise either commits or rollbacks. *)
  let run mode (store : int Store.t) conf =
    let create_count = from_log conf.ncreate in
    let create_mask = create_count - 1 in
    let read_count = from_log conf.nread in
    let write_count = from_log conf.nwrite in
    let refs = Array.init create_count (fun i -> Store.Ref.make store i) in
    let do_reads refs _round =
      for i = 1 to read_count do
        let r = refs.(i land create_mask) in
        ignore (Sys.opaque_identity (Store.Ref.get (Sys.opaque_identity store) r));
      done
    in
    let do_writes refs round =
      for i = 1 to write_count do
        let r = refs.(i land create_mask) in
        Store.Ref.set store r (round + i);
      done
    in
    let rec loop round =
      if round = conf.rounds then ()
      else match mode with
      | `Commit ->
        (* succesful transaction *)
        Store.tentatively store (fun () ->
          do_reads refs round;
          do_writes refs round;
          loop (round + 1)
        )
      | `Abort ->
        match
          Store.tentatively store (fun () ->
            do_reads refs round;
            do_writes refs round;
            loop (round + 1);
            raise Exit
          )
        with
        | () -> assert false
        | exception Exit -> ()
    in loop 0

  let main mode conf =
    run mode (Sys.opaque_identity (Store.create ())) conf
end


let int_of_env var =
  try int_of_string (Sys.getenv var)
  with _ ->
    Printf.ksprintf failwith
      "Expected a number for environment variable %s"
      var

let conf = {
  ncreate = Log (int_of_env "NCREATE");
  nread = Log (int_of_env "NREAD");
  nwrite = Log (int_of_env "NWRITE");
  rounds = int_of_env "ROUNDS";
}

let assoc_of_env var assoc =
  let fail () =
    Printf.ksprintf failwith
      "Expected environment variable %s in [%s]"
      var
      (String.concat " | " (List.map fst assoc))
  in
  try List.assoc (Sys.getenv var) assoc
  with _ -> fail ()

let bench =
  assoc_of_env "BENCH" [
    "Raw", Raw;
    "Transactional-raw", (Transaction `Raw);
    "Transactional-abort", (Transaction `Abort);
    "Transactional-commit", (Transaction `Commit);
    "Backtracking-abort", Backtracking (`Abort, `Semi_persistent);
    "Backtracking-commit", Backtracking (`Commit, `Semi_persistent);
    "Backtracking-persistent", Backtracking (`Abort, `Persistent);
  ]

let impl =
  assoc_of_env "IMPL" [
    "Ref", Ref;
    "TransactionalRef", TransactionalRef;
    "BacktrackingRef", BacktrackingRef;
    "Facile", Facile;
    "Colibri2", Colibri2;
    "Store", Store;
    "Vector", Vector;
    "Map", Map;
  ]

let unsupported () =
  Printf.ksprintf failwith
    "Unsupported combination BENCH=%s IMPL=%s"
    (Sys.getenv "BENCH") (Sys.getenv "IMPL")

let () =
  (* Note: the redundancy in the cases below
     is intentional, we are trying to make sure
     that flambda will inline the functors away. *)
  match bench with
  | Raw ->
    begin match impl with
      | Ref ->
        let module B = BenchStore(StoreRef) in
        B.main conf

      | TransactionalRef ->
        let module B = BenchStore(StoreTransactionalRef) in
        B.main conf

      | BacktrackingRef ->
        let module B = BenchStore(StoreBacktrackingRef) in
        B.main conf

      | Facile ->
        let module B = BenchStore(StoreFacile) in
        B.main conf

      | Colibri2 ->
        let module B = BenchStore(StoreColibri2) in
        B.main conf

      | Store ->
        let module B = BenchStore(StoreBasile) in
        B.main conf

      | Map ->
        let module B = BenchStore(StoreMap) in
        B.main conf

      | Vector ->
        let module B = BenchStore(StoreVector) in
        B.main conf
   end

  | Transaction mode ->
    begin match impl with
      | Ref -> unsupported ()

      | TransactionalRef ->
        let module B =
          BenchStoreTransactional(StoreTransactionalRef) in
        B.main mode conf

      | BacktrackingRef ->
        let module B =
          BenchStoreTransactional(StoreBacktrackingRef) in
        B.main mode conf

      | Facile ->
        let module B =
          BenchStoreTransactional(StoreFacile) in
        B.main mode conf

      | Colibri2 ->
        let module B =
          BenchStoreTransactional(StoreColibri2) in
        B.main mode conf

      | Vector ->
        let module B =
          BenchStoreTransactional(StoreVector) in
        B.main mode conf

      | Map ->
        let module B =
          BenchStoreTransactional(StoreMap) in
        B.main mode conf

      | Store ->
        let module B =
          BenchStoreTransactional(StoreBasile) in
        B.main mode conf
    end

  | Backtracking (mode, `Semi_persistent) ->
    begin match impl with
      | Ref
      | TransactionalRef ->
        unsupported ()

      | BacktrackingRef ->
        let module B =
          BenchStoreBacktracking(StoreBacktrackingRef) in
        B.main mode conf

      | Vector ->
        let module B =
          BenchStoreBacktracking(StoreVector) in
        B.main mode conf

      | Map ->
        let module B =
          BenchStoreBacktracking(StoreMap) in
        B.main mode conf

      | Facile ->
        let module B =
          BenchStoreBacktracking(StoreFacile) in
        B.main mode conf

      | Colibri2 ->
        let module B =
          BenchStoreBacktracking(StoreColibri2) in
        B.main mode conf

      | Store ->
        let module B =
          BenchStoreBacktracking(StoreBasile) in
        B.main mode conf
   end

  | Backtracking (mode, `Persistent) ->
    begin match impl with
      | Ref
      | TransactionalRef
      | BacktrackingRef
      | Facile
      | Colibri2
        ->
        unsupported ()

      | Vector ->
        let module B =
          BenchStoreBacktracking(StoreVector) in
        B.main mode conf

      | Map ->
        let module B =
          BenchStoreBacktracking(StoreMap) in
        B.main mode conf

      | Store ->
        let module S = struct
          include StoreBasile
          let tentatively s f =
            let saved = capture s in
            match f () with
            | v -> v
            | exception exn ->
              let b = Printexc.get_raw_backtrace() in
              restore s saved;
              Printexc.raise_with_backtrace exn b
        end in
        let module B =
          BenchStoreBacktracking(S) in
        B.main mode conf
    end
