(***************************************************************************)
(*                                                                         *)
(*                                 UnionFind                               *)
(*                                                                         *)
(*                       Gabriel Scherer, Inria Saclay                     *)
(*                                                                         *)
(*  Copyright Inria. All rights reserved. This file is distributed under   *)
(*  the terms of the GNU Library General Public License version 2, with a  *)
(*  special exception on linking, as described in the file LICENSE.        *)
(***************************************************************************)


(**In a semi-persistent implementation, there are several "versions"
   of the same mutable structures (in our case: the references of
   a store). The references in the semi-persistent store may have
   a different value in each version; seen otherwise, each version
   has an independent logical "state" that maps references to values.

   There is a "current" (most recent) version. Reading and writing
   a reference reads and writes the state of the current
   verision. (There is no way to read or write a reference at an
   another version.)

   We can "branch" a new version that is a child of the current
   version, and becomes the new current version. The new version
   starts in exactly the same state as its parent version -- the
   previous current version.

   Wwe can "terminate" the current version, if it has a parent
   version. This parent version becomes the new current version.

   Note: versions form a linear path, not a tree. We would have a tree
   if we could branch from any version, not just the current one, in
   particular we could have a version with several children. This
   would correspond to a fully persistent implementation.

   There is a "root version" that is the current version when the
   store is just created, and has no parent version.
*)

module Pos_option : sig
  type t = private int
  val none : t
  val is_none : t -> bool
  val make : int -> t
end = struct
  type t = int
  let none = -1
  let is_none n = (n < 0)
  let make n =
    if n < 0 then invalid_arg "Pos_option.of_nat";
    n
end


(**Our implementation maintains a "global state", which is the state
   of all references in the store. (References are implemented record
   with mutable fields, this state is exactly their content.)

   [global state] invariant: the global state of our implementation
   is exactly the (logical) state of the "current version".
   (This guarantees that 'get' is fast, it is always just a read.)
*)
type 'a rref = {
  mutable current: 'a;
    (** The value of this reference in the current version. *)
  mutable last_record: Pos_option.t;
    (** [last_record] records the most recent occurrences of this
        reference recorded in the store journal. If this occurrence is
        part of the current version, then we can elide the write to
        the journal, we know that the value for the previous version
        is already recorded. *)
}

(**On the other hand, the logical state of other versions than the
   current version is not stored as-is anywhere in our
   implementation. Instead we store "diffs", which are themselves
   formed of "undo actions" carrying the necessary information to
   "undo" a single state change. *)
type undo_action =
  | Set : {
    ref: 'a rref;
      (** the reference that was written *)
    previous: 'a;
      (** the value of the reference before the write *)
    previous_record: Pos_option.t;
      (** the [last_record] value at write time *)
  } -> undo_action

type hstore = {
  journal: undo_action Stack.t;
  (** The undo actions of all diffs for all versions are stored in
     a single stack [journal], from the most recent to the oldest. *)
  mutable children: int list;
  (**For each child (non-root) version, the list [children] stores the
     index of this version in the [journal], that is, the starting
     position of the diff of this version. The children versions are
     stored in the list from the most recent (the current version) to
     the oldest (the immediate child of the root version).

     Note: The root version keeps no diff / undo log, so its immediate
     child always starts at index 0. *)
}

let new_store () = {
  journal = Stack.create ();
  children = [];
}

(* Copying is not supported. *)
let copy _s =
  assert false

let make (_s : hstore) (v : 'a) : 'a rref =
  {
    current = v;
    last_record = Pos_option.none;
  }

let get (_s : hstore) (x : 'a rref) : 'a =
  x.current

let slow_set s x last_record v =
  (* Add the write to the diff of the current version. *)
  let new_record = Stack.length s.journal in
  (* assert (new_record >= current_version_start); *)
  Stack.push (Set {
    ref = x;
    previous = x.current;
    previous_record = last_record;
  }) s.journal;
  x.last_record <- Pos_option.make new_record;
  x.current <- v

(**[set s x v] changes the state of the current version of [x] in [s] to be [v]. *)
let[@inline always] set (s : hstore) (x : 'a rref) (v : 'a) : unit =
  begin match s.children with
  | [] ->
    (* If there are no children, the current version is the root version
       which has no undo log. *)
    x.current <- v
  | current_version_start :: _ ->
    let last_record = x.last_record in
    if (last_record :> int) >= current_version_start then
      x.current <- v
    else
      slow_set s x last_record v
  end

let eq (_s : hstore) (x : 'a rref) (y : 'a rref) : bool =
  x == y

(**[branch s] creates a new version which becomes the current version;
   its state is the current version state, and its parent is the
   current version. *)
let branch s =
  s.children <- Stack.length s.journal :: s.children

(**[rollback s] modifies the state of the current version to go back to
   the state of its previous version. It does not terminate the
   current version, which remains the current version. *)
let rollback s =
  begin match s.children with
  | [] ->
    invalid_arg "rollback: the root version cannot be rolled back"
  | curr_start :: _ ->
    (* Before:
       positions:       Stack.length        curr_start
                            v                 v
                            ←  current diff   |
                            ^                 ^
       versions:        current ver.      parent ver.
       states:             S1                 S2

       We go back in time and 'rollback' each undo action beelonging
       to the diff of the current version, from the most recent to the
       oldest one. (The order matters, because undoing a recent write
       may get us back to the state of an older write in the same
       transaction, so this older write must be undone after and not
       before.)
    *)
    let current_version_length = Stack.length s.journal - curr_start in
    for _i = 1 to current_version_length do
      match Stack.pop s.journal with
      | Set { ref; previous; previous_record } ->
        ref.current <- previous;
        ref.last_record <- previous_record;
    done;
    assert (Stack.length s.journal = curr_start);
    (* After:
       positions:                Stack.length = curr_start
                                              v
                                             ←|
                                              ^
       versions:                 current ver. & parent ver.
       states:                      S2             S2
    *)
  end

(**[commit s] modifies the state of the parent of the current version,
   moving it to the state of the current version. It does not
   terminate the current version, which remains the current version. *)
let commit s =
  begin match s.children with
  | [] ->
    invalid_arg "rollback: the root version cannot be committed"
  | _curr_start :: parent_start :: rest ->
    (* Before:
       positions:       Stack.length        curr_start      parent_start
                            v                 v                v
                            ←  current diff   |   parent diff  |
                            ^                 ^                ^
       versions:        current ver.      parent ver.      parent parent ver.
       states:             S1                 S2              S3

       We move the boundary of our current version to the current
       stack length. This moves the content of our diff to our parent
       version, without changing the global state.
    *)
    s.children <- Stack.length s.journal :: parent_start :: rest;
    (* After:
       positions:       Stack.length                      parent_start
                            v                                  v
                           ←|    current diff + parent diff    |
                            ^                                  ^
       version:  curr. ver. & parent ver.                 parent parent ver.
       states:      S1           S1                            S3

       Note: it may be the case that some undo actions in the
       current_diff are below curr_start but above parent_start, in
       which case they end up as redundant undo actions (subsumed by
       another which is part of parent_diff) in the result. In general
       we don't have the invariant that all undo actions have
       a last_record below their version.
    *)
  | curr_start :: [] ->
    assert (curr_start = 0);
    (* The current version's parent is the root version, which has no
       undo log. We can discard the changes in our undo log instead of
       moving them to our parent.

       Before:
         positions:  Stack.length     curr_start = 0
                         v               v
                         ← current diff  |
         versions:    current ver.    root ver.
         states;        S1              S2

       There is one detail, however: clearing the undo actions
       invalidates the [last_record] field of the references recorded
       in those actions.
    *)
    s.journal |> Stack.iter (function
      | Set {ref; previous = _; previous_record = _} ->
        ref.last_record <- Pos_option.none;
    );
    Stack.clear s.journal;
    (* After:
         positions:     Stack.length = curr_start = 0
                                         v
                                        ←|
         versions:       current_version & root_version
         states;               S1              S1
    *)
  end

(**[terminate_nodiff s] terminates the current version, assuming that
   it contains no changes compared to its parent version -- typically
   one has called [rollback] or [commit] first. The parent version
   becomes the new current version. *)
let terminate_nodiff s =
  begin match s.children with
  | [] ->
    invalid_arg "terminate_nodiff: root version cannot be terminated"
  | curr_start :: rest ->
    assert (Stack.length s.journal = curr_start);
    s.children <- rest;
  end

let temporarily (s : hstore) (f : unit -> 'b) : 'b =
  branch s;
  Fun.protect ~finally:(fun () ->
    rollback s;
    terminate_nodiff s
  ) f

let tentatively (s : hstore) (f : unit -> 'b) : 'b =
  branch s;
  match f () with
  | v ->
    commit s;
    terminate_nodiff s;
    v
  | exception e ->
    let b = Printexc.get_raw_backtrace() in
    rollback s;
    terminate_nodiff s;
    Printexc.raise_with_backtrace e b


let debug (s : hstore) msg =
  Printf.eprintf "DEBUG %s\n%!" msg;
  let journal_len = Stack.length s.journal in
  let undo_actions =
    s.journal
    |> Stack.fold (fun li v -> v :: li) []
    |> Array.of_list
  in
  s.children |> List.fold_left (fun version_end version_start ->
    Printf.eprintf "TRANSACTION [%d; %d[:\t%!" version_start version_end;
    for i = version_start to version_end - 1 do
      match undo_actions.(i) with
      | Set { ref; previous; previous_record } ->
        Printf.eprintf "[%x: %d (%d)] "
          (Obj.magic ref)
          (Obj.magic previous)
          (previous_record :> int)
    done;
    prerr_endline ".";
    version_start
    ) journal_len
  |> (fun first_start -> assert (first_start = 0));


(* adapt to the new Store interface *)
type 'a t = hstore
type 'a store = 'a t
let create () = new_store ()
module Ref = struct
  type 'a t = 'a rref
  let make = make
  let get = get
  let set = set
end
