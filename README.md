# Store: Snapshottable Data Structures

`Store` is a library to add snapshotting capabilities to imperative data
structure with a low runtime cost and safe, user-friendly APIs. It is designed
for (and works best with) applications that exploit backtracking algorithms,
such as SMT solvers, type-checking and type-inference algorithms.

Currently, `Store` only provides snapshottable references; support for custom
built-in data structures that benefit from non-backtracked operations (such as
resizing a dynamic array) is planned.

The design of the Store library is described in the ICFP'24 paper:

    Clément Allain, Basile Clément, Alexandre Moine, and Gabriel Scherer. 2024.
    Snapshottable Stores.
    Proc. ACM Program. Lang. 8, ICFP, Article 248 (August 2024), 32 pages.
    https://doi.org/10.1145/3674637
