{
    outputs = {nixpkgs, self, ...}: let
        forAllSystems = fn:
            nixpkgs.lib.genAttrs [
                "x86_64-linux"
                "aarch64-linux"
            ] (system: fn system nixpkgs.legacyPackages.${system});

    in {
        packages = forAllSystems (system: pkgs: {
            default = self.packages.${system}.store;
            store = pkgs.callPackage ./nix/store.nix {};
        });

        overlays = forAllSystems (system: pkgs: {
            default = final: prev: {
                ocamlPackages = final.overrideScore (self: super: {
                    monolith = self.callPackage ./nix/monolith.nix { };
                });
            };
        });

        checks = forAllSystems (system: pkgs: {
            store = self.packages.${system}.store.overrideAttrs
                (oldAttrs: {
                    name = "check-${oldAttrs.name}";
                    doCheck = true;
                    # Skip installation
                    installPhase = "touch $out";
                });
        });

        devShells = forAllSystems (system: pkgs:
            let
                monolith = pkgs.ocamlPackages.callPackage ./nix/monolith.nix { };
            in
            {
                default = pkgs.mkShell {
                    nativeBuildInputs = with pkgs.ocamlPackages; [
                        ocaml
                        findlib
                        dune_3
                        ocaml-lsp
                    ];

                    buildInputs = with pkgs.ocamlPackages; [
                        pkgs.aflplusplus
                        monolith
                        odoc
                        odig
                    ];
            };
        });
    };
}
